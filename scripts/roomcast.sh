#!/bin/bash
mkdir -p ~/roomcast
cd ~/roomcast
git clone git@github.com:tlodge/roomcast.server.git
mkdir -p ~/roomcast/roomcast.server/static/js/
git clone git@github.com:tlodge/roomcast.panel.git
git clone git@github.com:tlodge/roomcast.dashboard.git
git clone git@github.com:tlodge/roomcast.auth.git
cd ~/roomcast/roomcast.server && ./setuplinks.sh
