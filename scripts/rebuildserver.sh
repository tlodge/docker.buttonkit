#!/bin/sh
cd ~/roomcast/roomcast.server
git pull
cp package.json ~/docker/buttonkit.server/conf
docker stop buttonkit1; docker rm buttonkit1;
docker rmi docker_buttonkit
cd ~/docker
docker-compose up -d
