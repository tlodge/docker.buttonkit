#!/bin/bash

if [ $# -ne 1 ]; then
    echo $0: usage: ./restore.sh [backupfile.tar.gz]
    exit 1
fi

name=$1
docker stop neo4j1
cd /opt/neo4j && sudo tar -zxf $name 
docker start neo4j1
