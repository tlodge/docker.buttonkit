#!/bin/sh
cd ~/docker/samples && git pull
cd ~/docker/samples/wharfside/neo4j 
cp accessgroups.cypher /opt/neo4j/schemas
cp wharfside.production.users.cypher /opt/neo4j/schemas/users.cypher
cp buttons.production.cypher /opt/neo4j/schemas/buttons.cypher
cp buttoncategories.cypher /opt/neo4j/schemas/
cp building.cypher /opt/neo4j/schemas/
cd ~/docker/neo4j && ./reset.sh
