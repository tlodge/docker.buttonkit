#!/bin/sh
sudo apt-get install -y bc
sudo git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt
sudo docker stop nginx1
cd /opt/letsencrypt/
./letsencrypt-auto certonly --standalone

