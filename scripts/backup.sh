docker stop neo4j1
name=$(date '+%y-%m-%d')
cd /opt/neo4j/ && sudo tar -zcvf "/opt/backups/neo4j/wharfside.$name.tar.gz" graph.db 
docker start neo4j1
cd /opt/mongo && sudo tar -zcvf "/opt/backups/mongo/wharfside.$name.tar.gz" . 
