re-issuing certs
----------------

This has not yet been automated.  To re-issue an expired cert go to

cd /opt/letsencrypt/
./letsencrypt-auto certonly --standalone

then when prompted give the full domain of the cert that you want to create eg:

wharfside.buttonkit.com

note that this will create the new cert files in:

/etc/letsencrypt/archive/[domainname]

eg

/etc/letsencrypt/archive/wharfside.buttonkit.com

it also will increment the suffix of the files by one, so 

ssl certificate will become fullchain[n+1].pem;
ssl_certificate_key will become privkey[n+1].pem;

make sure you update these values in the nginix defaul file in docker/nginx/conf

once done, restart nginx and you are good to go.
