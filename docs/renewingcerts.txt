docker stop nginx1 
cd /opt/letsencrypt
./letsencrypt-auto renew
[this will put a new cert in /etc/letsencrypt/archive (fullchain[n].pem and privkey[n].pem)]
the docker compose file currently links its /etc/letsencrypt/live/ dir to the hosts /etc/letsencrypt/archive dir, so need to change
/docker/nginx/conf/default to reflect the new cert names:

ssl_certificate /etc/letsencrypt/live/wharfside.buttonkit.com/fullchain[n].pem;
ssl_certificate_key /etc/letsencrypt/live/wharfside.buttonkit.com/privkey[n].pem;

then cd ~/docker
docker-compose build nginx
docker-compose up -d

