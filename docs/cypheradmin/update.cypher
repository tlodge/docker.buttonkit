MATCH (w:Webhook {webhookId:'0x549fe6e161800001'})
SET w.parameters = '{"static":[{"type":"datastore","id":"datastore","title":"name of the logstore you would like to display","value":{"storeId":"collected", "schema":[{"path":"1","alias":"user signature"},{"path":"0","alias":"picked up parcel(s)"},{"path":"2","alias":"staff member"}, {"path":"3","alias":"time picked up"}]}}],"dynamic":[]}'
RETURN w

