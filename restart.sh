#!/bin/bash

if [ $# -ne 1 ]; then
    echo 'usage ./restart.sh [dev|prod]'
    exit 1
fi

if [ $1 == 'prod' ]; then
    docker-compose up -d
fi

if [ $1 == 'dev' ]; then
    docker-compose  -f ./docker-compose.dev.yml up -d
fi
