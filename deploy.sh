#!/bin/sh
docker tag docker_mongo tlodge/mongo && docker push tlodge/mongo
docker tag docker_nginx tlodge/nginx && docker push tlodge/nginx
docker tag docker_postfix tlodge/postfix && docker push tlodge/postfix
docker tag docker_red tlodge/red && docker push tlodge/red
docker tag docker_redis tlodge/redis && docker push tlodge/redis
docker tag docker_neo4j tlodge/neo4j && docker push tlodge/neo4j
docker tag docker_worker tlodge/worker && docker push tlodge/worker
docker tag docker_rabbitmq tlodge/rabbitmq && docker push tlodge/rabbitmq
