to get started with redis, you need to copy the conf/redis.conf.sample to conf/redis.sample

create a strong password:

echo "somephrase" | sha256sum

and edit conf/redis.sample and set the password:

requirepass astrongpassword

to the hash that was spat out in the previous step

now go to ~/docker/buttonkit.server/conf

and edit config.js

to

redis:{
        host: 'redis',
        port: 6679,
        pass: '[hash spat out in previous step]'
},
