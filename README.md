#getting started

This will set up a full buttonkit installation on your host.  It consists of a bunch of docker containers. The main app server is in buttonkit.server and, in addition to providing a REREST backend, it will serve up client side javascript for the dashboard, panel, auth (login/registration).  Each of these are built from the main buttonkit code, which sits outside the containers so it can be modified without having to update a container.  The code is watched by the buttonkit.dashboard, buttonkit.panel, buttonkit.login and buttonkit.register containers which will recompile on any change.

#pull in the code

cd docker/scripts
./roomcast.sh

#update the buttonkit.server config
cd docker/buttonkit.server/conf
cp config.sample.js config.js

then update config.js with appropriate settings!

#check the nginx cert stuff (defaults to ssl - change the docker/nginx/default to run over just http for testing:

change listen 443 ssl;
to listen 80;

and comment out the cert stuff:
#ssl_certificate /etc/letsencrypt/live/wharfside.buttonkit.com/fullchain1.pem;
#ssl_certificate_key /etc/letsencrypt/live/wharfside.buttonkit.com/privkey1.pem;

#build containers

cd docker
sudo docker-compose up -d

#a note on file space
on 512Mb 20Gb droplet, found that the node-red container wouldn't build (memory issue) so need to create a swapfile as follows (https://digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04):
 
  sudo fallocate -l 4G /swapfile
  sudo swapon -s
  sudo chmod 600 /swapfile
  sudo mkswap /swapfile
  sudo swapon /swapfile
  sudo swapon -s
  sudo vi /etc/fstab
  
put the following at end:
 
  /swapfile none swap sw 0 0
