#!/bin/sh
docker stop neo4j1 
docker rm neo4j1 
now=$(date +%s)
sudo mv /opt/neo4j/graph.db /opt/neo4j/removed/graph_$now.db
docker-compose up -d
sleep 15
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file $1 
