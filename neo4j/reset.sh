#!/bin/sh
docker stop neo4j1 
docker rm neo4j1 
now=$(date +%s)
sudo mv /opt/neo4j/graph.db /opt/neo4j/removed/graph_$now.db
./runit.sh
sleep 15
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file /data/neo4j/schemas/building.cypher
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file /data/neo4j/schemas/users.cypher
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file /data/neo4j/schemas/accessgroups.cypher
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file /data/neo4j/schemas/buttoncategories.cypher
docker exec neo4j1 /var/lib/neo4j/bin/neo4j-shell -file /data/neo4j/schemas/buttons.cypher
docker stop neo4j1
docker rm neo4j1
docker-compose up -d
